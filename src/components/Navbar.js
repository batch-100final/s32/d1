// base imports
import React, { useContext, Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';

// bootstrap components
import { Navbar, Nav } from 'react-bootstrap';

import UserContext from '../UserContext';

// export default allows the function to be used in other files
export default function NavBar() {
    const { user } = useContext(UserContext);

    return (
    <Navbar bg="light" expand="lg">
		<Navbar.Brand as={Link} to="/">React-Bootstrap</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
 				<Nav className="ml-auto">
 					<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
      				<Nav.Link as={NavLink} to="/courses">courses</Nav.Link>
              {(user.email !== null) ? <Nav.Link as={NavLink} to="/logout">logout</Nav.Link>
                :
                <Fragment>
                  <Nav.Link as={NavLink} to="/login">login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">register</Nav.Link>
                </Fragment>
               }
 				</Nav>
 			</Navbar.Collapse>
		</Navbar>
    )
}