export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "First Description",
		price: 45000,
		onOffer: true,
		start_date: "2021-02-20",
		end_date: "2021-08-19"
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Second Description",
		price: 50000,
		onOffer: true,
		start_date: "2021-12-12",
		end_date: "2021-11-11"
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Third Description",
		price: 60000,
		onOffer: true,
		start_date: "2021-05-11",
		end_date: "2021-05-66"
	}
]