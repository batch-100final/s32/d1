// base imports
import React, {Fragment} from 'react';

// bootstrap components
import Container from 'react-bootstrap/Container';

// app components
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// export default allows the function to be used in other files
export default function Home(){
	return(
		<Fragment>
				<Banner />
			<Container>	
				<Highlights />
			</Container>
		</Fragment>
	)
}

