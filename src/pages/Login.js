// Base Imports
import React, { useState, useEffect, useContext } from 'react';

// Bootstrap Components
// import Form from 'react-bootstrap/Form';
// import Container from 'react-bootstrap/Container';
// import Button from 'react-bootstrap/Button';
import { Form, Button, Container } from 'react-bootstrap';


import UserContext from '../UserContext';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

     function login(e) {
        // prevents page redirection
        e.preventDefault();
        alert(`${email}, You are now logged in.`);
        console.log(user);
        // clear the input fields
        setEmail('');
        setPassword('');
    }

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        //Determine if all conditions have been met
        if (isEmailNotEmpty && isPasswordNotEmpty) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }

    }, [email, password]);

	return (
		<Container fluid>
            <h3>Login</h3>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)} 
                        required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password} 
                        onChange={(e) => setPassword(e.target.value)} 
                        required
                    />
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>
                    Login
                </Button>
            </Form>
        </Container>
	)
}