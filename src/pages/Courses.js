// base imports
import React, {Fragment} from 'react';

// app components
import Course from '../components/Course';

// app data
import coursesData from '../data/courses';

export default function Courses(){
	const CourseCards = coursesData.map((course) => {
		return <Course key = {course.id} course = {course} />
	})
	return(
		<Fragment>
			{CourseCards}
		</Fragment>
	)
}
